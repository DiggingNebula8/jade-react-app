# Just Another Development Enterprise React Web-app v0.1.0a

Website developed for a web agency. [itsJade](https://itsjade.in/) - aims to lead a diversified business with IT, Media, and Not for profit organisations. Our business intention is to provide services such as IT Solutions, Game Development, Product Subscriptions, and essential Pop culture Involvement. Our mission is simple: help professionals in achieving their goals whilst doing the same with ourselves.

## Design Choices

### Design Changes

The following are the changes we've made whilst developing this product:

1. Dropped Bootstrap 4 and opted React.
2. Choosing a dark-themed modern animation based design.

### Design Workflow

Chosen the following workflow to make our project eloquent.

- _Empathise_ - the needs - because it allows us to set aside one's opinions and gain real insight into users and their needs.
- _Define_ - the accumulated information - to analyse the observations and synthesise them to establish the core problem.
- _Ideate_ - generate ideas - to solve the problems with the rooted background of knowledge gathered in the past stages. By seeking an alternative way to view the problem and identify innovative solutions.
- _Prototype_ - create solutions - several inexpensive, scaled-down versions of the product until we finalise an exceptional solution.
- _Test_ - try the solution - the product rigorously using the best solutions identified in the Prototype phase. Whilst being flexible to return to previous stages in the process to make further iterations, alterations and refinements to rule out alternative solutions.
- _Build_ - finalise the solution - get feedbacks and present the build.

## Getting Started

Since its a website, the mandetory prerequsites are quite less. However, having a good project structure is a vital whilst development any application. _Current version: v0.1.0a_

### Prerequisites

```
Node.js
React.js
SASS Compiler
FontAwesome
GSAP3
ESlint
Prettier
```

### Installing

Clone the project

```
1. cd to your project directory
2. git clone https://DiggingNebula8@bitbucket.org/DiggingNebula8/jade-react-app.git
```

Install the Dependencies

```
npm install
```

## Built With

- [Node JS](https://nodejs.org/en/) - JavaScript runtime
- [React JS](https://reactjs.org/) - A JavaScript library for building user interfaces

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository]().

## Authors

- **Siva Vadlamani** - [itsJade](https://bitbucket.org/account/user/DiggingNebula8/)

## License

This project is licensed under the Creative Commons Attribution Share Alike 4.0 **cc-by-sa-4.0** - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks to

- Gary from Design Course
- Folks at Interaction Design Foundation
- DevEd
