import React, { useEffect } from 'react'
import gsap from 'gsap'
import './App.scss'
import Navbar from './components/Navbar'
import Home from './components/pages/Home'
import Work from './components/pages/Work'
import About from './components/pages/About'
import Contact from './components/pages/Contact'
import Shop from './components/pages/Shop'
import Footer from './components/Footer'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ScrollToTopBtn from './components/utilites/ScrollToTop'

//----FontAwesome Setup----//

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'

library.add(far, fas, fab)
//----FontAwesome Setup End----//

function App() {
  useEffect(() => {
    let vh = window.innerHeight * 0.01
    document.documentElement.style.setProperty('--vh', `${vh}px`)

    gsap.to('body', 0, { css: { visibility: 'visible' } })
  }, [])

  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/Work" exact component={Work} />
          <Route path="/About" exact component={About} />
          <Route path="/Contact" exact component={Contact} />
          <Route path="/Shop" exact component={Shop} />
        </Switch>
        <ScrollToTopBtn />
        <Footer />
      </div>
    </Router>
  )
}

export default App
