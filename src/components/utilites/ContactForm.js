import React, { Component } from 'react'

export default class ContactForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      message: '',
    }
  }

  render() {
    return (
      <div className="contact-form">
        <form
          id="contact-form"
          autocomplete="off"
          onSubmit={this.handleSubmit.bind(this)}
          method="POST"
        >
          <div className="form-group">
            <input
              type="text"
              required
              placeholder=" "
              className="form-control"
              value={this.state.name}
              onChange={this.onNameChange.bind(this)}
            />
            <label className="label-class" htmlFor="name">
              <span className="label-text">Name</span>
            </label>
          </div>
          <div className="form-group">
            <input
              type="email"
              required
              placeholder=" "
              className="form-control"
              aria-describedby="emailHelp"
              value={this.state.email}
              onChange={this.onEmailChange.bind(this)}
            />
            <label className="label-class" htmlFor="exampleInputEmail1">
              <span className="label-text">Email</span>
            </label>
          </div>
          <div className="form-group">
            <textarea
              className="form-control"
              placeholder=" "
              rows="5"
              value={this.state.message}
              onChange={this.onMessageChange.bind(this)}
            />
            <label className="label-class" htmlFor="message">
              <span className="label-text">Message</span>
            </label>
          </div>
          <button type="submit" className="text-light button-primary">
            Submit
          </button>
        </form>
      </div>
    )
  }

  onNameChange(event) {
    this.setState({ name: event.target.value })
  }

  onEmailChange(event) {
    this.setState({ email: event.target.value })
  }

  onMessageChange(event) {
    this.setState({ message: event.target.value })
  }

  handleSubmit(event) {}
}
