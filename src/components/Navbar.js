import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function Navbar() {
  return (
    <div>
      <div className="container">
        <nav>
          <a className="logo" href="/">
            J D
          </a>

          <ul>
            <li>
              <a href="/About">About</a>
            </li>
            <li>
              <a href="/Work">Work</a>
            </li>
            <li>
              <a href="/Contact">Contact</a>
            </li>
            <li>
              <a href="/Shop">
                <FontAwesomeIcon icon={['fas', 'store']} />
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Navbar
