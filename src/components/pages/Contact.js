import React, { useEffect } from 'react'
import gsap from 'gsap'
import ContactForm from '../utilites/ContactForm'

function Contact() {
  useEffect(() => {
    const tl = gsap.timeline()

    tl.from('.line span', 1.8, {
      y: 100,
      ease: 'power4.out',
      delay: 1,
      skewY: 3,
      stagger: {
        amount: 0.3,
      },
    })
  }, [])
  return (
    <div>
      <div className="container">
        <div className="wrapper">
          <div className="hero-content">
            <h1>
              <div className="line">
                <span>Hey, want to</span>
              </div>
              <div className="line">
                <span>Know more?</span>
              </div>
              <div className="line">
                <span>Talk to us.</span>
              </div>
            </h1>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-monochromatic">
        <div className="container">
          <div className="subhero-content">
            <h2>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
              ratione, quibusdam facilis omnis soluta sint nulla fugiat unde nostrum
              odio.
            </h2>
          </div>
        </div>
      </div>

      <div className="container-fluid">
        <div className="container">
          <div className="features">
            <h1>How We Do Things</h1>
            <ContactForm />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact
