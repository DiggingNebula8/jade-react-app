import React, { useEffect } from 'react'
import gsap from 'gsap'

function Shop() {
  useEffect(() => {
    const tl = gsap.timeline()

    tl.from('.line span', 1.8, {
      y: 100,
      ease: 'power4.out',
      delay: 1,
      skewY: 3,
      stagger: {
        amount: 1,
      },
    })
  }, [])
  return (
    <div>
      <div className="container">
        <div className="wrapper">
          <div className="hero-content">
            <h1>
              <div className="line">
                <span>Coming Soon</span>
              </div>
              <div className="line">
                <span>-ish</span>
              </div>
            </h1>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-monochromatic">
        <div className="container">
          <div className="subhero-content">
            <h2>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
              ratione, quibusdam facilis omnis soluta sint nulla fugiat unde nostrum
              odio.
            </h2>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Shop
