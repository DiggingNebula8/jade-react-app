import React from 'react'

function How() {
  return (
    <div className="container">
      <div>
        <h1>How</h1>
        <h3>Graphic</h3>
      </div>
      <div>
        <h1>Explanation</h1>
        <h3>Empathise</h3>
        <h3>Define</h3>
        <h3>Ideate</h3>
        <h3>Prototype</h3>
        <h3>Test</h3>
        <h3>Build</h3>
      </div>
    </div>
  )
}

export default How
