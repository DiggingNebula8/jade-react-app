import React, { useEffect } from 'react'
import gsap from 'gsap'

function Work() {
  useEffect(() => {
    const tl = gsap.timeline()

    tl.from('.line span', 1.8, {
      y: 100,
      ease: 'power4.out',
      delay: 1,
      skewY: 3,
      stagger: {
        amount: 0.3,
      },
    })
  }, [])
  return (
    <div>
      <div className="container">
        <div className="wrapper">
          <div className="hero-content">
            <h1>
              <div className="line">
                <span>We work hard</span>
              </div>
              <div className="line">
                <span>yes, sure.</span>
              </div>
            </h1>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-monochromatic">
        <div className="container">
          <div className="subhero-content">
            <h2>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
              ratione, quibusdam facilis omnis soluta sint nulla fugiat unde nostrum
              odio.
            </h2>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-light">
        <div className="container">
          <div className="features">
            <h1>How We Do Things</h1>
            <div className="features-grid">
              <div className="feature">
                <h2>Empathise</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur{' '}
                  <strong>Human-Centred design-based approach</strong> adipisicing
                  elit. Est fuga neque quos, qui iure mollitia suscipit consectetur
                  nemo. Nisi nesciunt dignissimos et veniam. Quaerat doloremque atque
                  sunt dolorum porro deserunt!
                </p>
              </div>
              <div className="feature">
                <h2>Define</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.{' '}
                  <strong>HTML&SCSS</strong> Eos sunt cum hic fugit earum delectus.
                  Quo ipsa quam <strong>React Framework</strong> assumenda, omnis
                  impedit, quidem ullam sequi eos deserunt eaque vel, tempora fugiat.
                </p>
              </div>
              <div className="feature">
                <h2>Ideate</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur{' '}
                  <strong>Unreal Engine</strong> adipisicing elit. Itaque officia,
                  tempora at illo necessitatibus <strong>Shader graph & HLSL</strong>{' '}
                  temporibus! Fugiat consectetur non eius ipsam perferendis!
                  Consequatur ad <strong>Lighting & Rendering</strong> quisquam a
                  atque mollitia doloremque hic debitis.
                </p>
              </div>
              <div className="feature">
                <h2>Prototype</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos sunt
                  cum hic fugit earum delectus? Quo <strong>Atlassian Jira</strong>{' '}
                  ipsa quam assumenda, omnis impedit, quidem ullam sequi eos deserunt
                  eaque vel, tempora fugiat.
                </p>
              </div>
              <div className="feature">
                <h2>Test</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos sunt
                  cum hic fugit earum delectus? Quo <strong>Atlassian Jira</strong>{' '}
                  ipsa quam assumenda, omnis impedit, quidem ullam sequi eos deserunt
                  eaque vel, tempora fugiat.
                </p>
              </div>
              <div className="feature">
                <h2>Build</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos sunt
                  cum hic fugit earum delectus? Quo <strong>Atlassian Jira</strong>{' '}
                  ipsa quam assumenda, omnis impedit, quidem ullam sequi eos deserunt
                  eaque vel, tempora fugiat.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Work
