import React, { useEffect } from 'react'
import gsap from 'gsap'
import heroImg from '../../images/hero-section.jpg'
import ContactForm from '../utilites/ContactForm'

function Home() {
  useEffect(() => {
    const tl = gsap.timeline()

    tl.from('.line span', 1.8, {
      y: 100,
      ease: 'power4.out',
      delay: 1,
      skewY: 3,
      stagger: {
        amount: 0.3,
      },
    })
      .from('.hero-image img', 1.2, {
        opacity: 0,
        x: 100,
        ease: 'expo.inOut',
        delay: 0.3,
      })
      .from('.hero-image a', 1, {
        opacity: 0,
        ease: 'Power4.easeInOut',
      })
  }, [])

  return (
    <div>
      <div className="container">
        <div className="wrapper">
          <div className="hero-content">
            <h1>
              <div className="line">
                <span>We Build Top Tier</span>
              </div>
              <div className="line">
                <span>Designs, Look</span>
              </div>
              <div className="line">
                <span>For yourself.</span>
              </div>
            </h1>
          </div>
          <div className="hero-image">
            <img src={heroImg} alt="" />
            <div className="container">
              <a role="button" className="text-light button-complementary" href="/">
                All Projects
              </a>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-monochromatic">
        <div className="container">
          <div className="subhero-content">
            <h2>
              Looking for quick solutions to speed up your workflow? We've got you
              covered, have a look at our pre-made plugins and templates.
            </h2>
            <a role="button" className="button-dark" href="/">
              Shop
            </a>
          </div>
        </div>
      </div>
      <div className="container-fluid bg-light">
        <div className="container">
          <div className="features">
            <h1>Here's what we provide, we provide happily. </h1>
            <div className="features-grid">
              <div className="feature">
                <h2>UX Solutions</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur{' '}
                  <strong>Human-Centred design-based approach</strong> adipisicing
                  elit. Est fuga neque quos, qui iure mollitia suscipit consectetur
                  nemo. Nisi nesciunt dignissimos et veniam. Quaerat doloremque atque
                  sunt dolorum porro deserunt!
                </p>
              </div>
              <div className="feature">
                <h2>Web development</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.{' '}
                  <strong>HTML&SCSS</strong> Eos sunt cum hic fugit earum delectus.
                  Quo ipsa quam <strong>React Framework</strong> assumenda, omnis
                  impedit, quidem ullam sequi eos deserunt eaque vel, tempora fugiat.
                </p>
              </div>
              <div className="feature">
                <h2>Shader Graphics</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur{' '}
                  <strong>Unreal Engine</strong> adipisicing elit. Itaque officia,
                  tempora at illo necessitatibus <strong>Shader graph & HLSL</strong>{' '}
                  temporibus! Fugiat consectetur non eius ipsam perferendis!
                  Consequatur ad <strong>Lighting & Rendering</strong> quisquam a
                  atque mollitia doloremque hic debitis.
                </p>
              </div>
              <div className="feature">
                <h2>Project Management</h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos sunt
                  cum hic fugit earum delectus? Quo <strong>Atlassian Jira</strong>{' '}
                  ipsa quam assumenda, omnis impedit, quidem ullam sequi eos deserunt
                  eaque vel, tempora fugiat.
                </p>
              </div>
            </div>
            <div className="text-align-end">
              <a role="button" className="button-complementary" href="/Work">
                See More
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <h1>Stay Connected</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste dolore omnis
          odit id accusamus consectetur!
        </p>
        <ContactForm />
      </div>
    </div>
  )
}

export default Home
