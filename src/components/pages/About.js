import React, { useEffect } from 'react'
import gsap from 'gsap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function About() {
  useEffect(() => {
    const tl = gsap.timeline()

    tl.from('.line span', 1.8, {
      y: 100,
      ease: 'power4.out',
      delay: 1,
      skewY: 3,
      stagger: {
        amount: 0.3,
      },
    })
  }, [])
  return (
    <div>
      <div className="container">
        <div className="wrapper">
          <div className="hero-content">
            <h1>
              <div className="line">
                <span>A couple of mates</span>
              </div>
              <div className="line">
                <span> living in the</span>
              </div>
              <div className="line">
                <span>mountains, bulding</span>
              </div>
              <div className="line">
                <span>amazing designs.</span>
              </div>
            </h1>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-monochromatic">
        <div className="container">
          <div className="subhero-content">
            <h2>
              We aim to lead diversified business with IT, Media, and Not for profit
              organisations. Our business intention is to provide eloquent IT
              Solutions and Product Subscriptions for Web and Game Development based
              work. Our mission is simple: help professionals in achieving their
              goals whilst doing the same with ourselves.
            </h2>
          </div>
        </div>
      </div>

      <div className="container-fluid bg-light">
        <div className="container">
          <div className="features">
            <h1>Meet the team</h1>
            <div className="features-grid">
              <div className="feature">
                <h2>
                  <FontAwesomeIcon icon={['fas', 'code']} /> Siva Vadlamani
                </h2>
                <p>
                  Siva Vadlamani, the lead developer of itsJade, has four years of
                  experience in writing code. He has been writing code to develop
                  things that he loves: web design and development, game mechanics,
                  lighting and rendering, and shader scripting.
                </p>
              </div>
              <div className="feature">
                <h2>
                  <FontAwesomeIcon icon={['fas', 'tasks']} /> Puspendu Mandal
                </h2>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos sunt
                  cum hic fugit earum delectus? Quo ipsa quam assumenda, omnis
                  impedit, quidem ullam sequi eos deserunt eaque vel, tempora fugiat.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About
