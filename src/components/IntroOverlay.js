import React from 'react'

function IntroOverlay() {
  return (
    <div className="intro-overlay">
      <div className="total-page">
        <div className="overlay"></div>
      </div>
    </div>
  )
}

export default IntroOverlay
