import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function Footer() {
  return (
    <div className="container">
      <div className="footer">
        <div className="sitemap">
          <div className="products">
            <h4>Products</h4>
            <ul>
              <li>
                <a href="/">Web Templates</a>
              </li>
              <li>
                <a href="/">Unreal Plugins</a>
              </li>
              <li>
                <a href="/">Features</a>
              </li>
            </ul>
          </div>
          <div className="community">
            <h4>Community</h4>
            <ul>
              <li>
                <a href="/">Our Work</a>
              </li>
              <li>
                <a href="/">Tech Guides</a>
              </li>
              <li>
                <a href="/">Our Brands</a>
              </li>
            </ul>
          </div>
          <div className="support">
            <h4>Support</h4>
            <ul>
              <li>
                <a href="/">Forums</a>
              </li>
              <li>
                <a href="/">Purchase Support</a>
              </li>
              <li>
                <a href="/">Technical Support</a>
              </li>
              <li>
                <a href="/">FAQ</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="post-sitemap">
          <div className="socials">
            <h4>Follow Us</h4>
            <ul>
              <li>
                <a href="/">
                  <FontAwesomeIcon icon={['fab', 'facebook']} />
                </a>
              </li>
              <li>
                <a href="/">
                  <FontAwesomeIcon icon={['fab', 'instagram']} />
                </a>
              </li>
              <li>
                <a href="/">
                  <FontAwesomeIcon icon={['fab', 'bitbucket']} />
                </a>
              </li>
              <li>
                <a href="/">
                  <FontAwesomeIcon icon={['fab', 'patreon']} />
                </a>
              </li>
            </ul>
          </div>

          <div className="site-info">
            <h6>Built with React</h6>
            <h6>Copyright &copy; 2020 itsJade Inc. All right reserved.</h6>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
